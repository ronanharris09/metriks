import { BundleAnalyzerPlugin } from "webpack-bundle-analyzer"

export default (config, env, helpers) => {
    // Disabling size-plugin reports from producing any size-plugin*.json at build
    const sizePlugin = helpers.getPluginsByName(config, "SizePlugin")[0]
    if (sizePlugin) {
        config.plugins[sizePlugin.index].options = {
            publish: false,
            writeFile: false,
        }
    }

    // Overriding Preload strategy from 'media' to 'default'
    const critters = helpers.getPluginsByName(config, "Critters")[0]
    if (critters) {
        critters.plugin.options.preload = "default"
    }

    // Disabling source maps output at build
    if (env.production) {
        config.devtool = false
    }

    // Adding plugins to webpack
    config.plugins.push(
        new BundleAnalyzerPlugin({
            analyzerMode: "static",
            defaultSizes: "gzip",
            excludeAssets: "./build/assets",
            logLevel: "error",
            openAnalyzer: false,
            reportFilename: "./webpack/index.html",
        })
    )
}
