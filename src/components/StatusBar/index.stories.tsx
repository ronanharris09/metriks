import { h } from "preact"
import { Meta, Story } from "@storybook/preact"
import StatusBar, { Props } from "./"

export default {
    title: "Metriks/StatusBar",
    component: StatusBar,
} as Meta<typeof StatusBar>

const Template: Story<Props> = (args) => <StatusBar {...args} />

export const Default = Template.bind({})
Default.args = {}
