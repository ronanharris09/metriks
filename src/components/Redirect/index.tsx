import { h } from "preact"
import { route } from "preact-router"
import { useEffect } from "preact/hooks"
import PropTypes, { InferProps } from "prop-types"

const propTypes = {
    path: PropTypes.string.isRequired,
    to: PropTypes.string.isRequired,
}

export type Props = InferProps<typeof propTypes>

const Redirect = ({ path, to }: Props) => {
    useEffect(() => {
        route(to, true)
    }, [])

    return null
}

Redirect.propTypes = propTypes

Redirect.defaultProps = {
    path: "/",
    to: "/current",
}

export default Redirect
