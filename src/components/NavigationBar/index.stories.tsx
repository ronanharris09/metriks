import { h } from "preact"
import { Meta, Story } from "@storybook/preact"
import NavigationBar, { Props } from "./"

export default {
    title: "Metriks/NavigationBar",
    component: NavigationBar,
} as Meta<typeof NavigationBar>

const Template: Story<Props> = (args) => (
    <div class="py-96 bg-white w-full">
        <NavigationBar {...args} />
    </div>
)

export const Default = Template.bind({})
Default.args = {}
