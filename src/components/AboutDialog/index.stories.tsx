import { h } from "preact"
import { Meta, Story } from "@storybook/preact"
import AboutDialog, { Props } from "./"

export default {
    title: "Metriks/FileDialog",
    component: AboutDialog,
} as Meta<typeof AboutDialog>

const Template: Story<Props> = (args) => <AboutDialog {...args} />

export const Default = Template.bind({})
Default.args = {}
