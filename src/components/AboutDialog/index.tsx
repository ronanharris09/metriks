import { h, JSX, Fragment } from "preact"
import { useContext } from "preact/hooks"
import PropTypes, { InferProps } from "prop-types"
import { StoreContext } from "../../context"
import { ACTION, ACCESSKEY } from "../../constants"
import Dialog from "../Dialog"

const propTypes = {}

export type Props = InferProps<typeof propTypes>

const AboutDialog = (): JSX.Element => {
    const { store, dispatch } = useContext(StoreContext)

    const dialogStateHandler = () => {
        let prevState = store
        prevState.visual.dialog.about = !prevState.visual.dialog.about

        dispatch({
            type: ACTION.STORE.VISUAL,
            body: prevState,
        })
    }

    const loadToBuffer = (i: number) => (window.location.href = `/${i}`)

    return (
        <>
            <Dialog
                title="About App"
                description="Details on description, backstory, author, and license"
                isOpen={store.visual.dialog.about}
                callback={() => dialogStateHandler()}
                accessKey={ACCESSKEY.DIALOG.ABOUT}
            >
                <>
                    <article class="p-4 w-11/12">
                        <h2 class="text-xl font-bold">Description</h2>
                        <p>
                            Metriks is a web application built on top of Preact,
                            TailwindCSS CLI, and slow-json-stringify as a
                            frontend for another project of mine named TreaTS to
                            allow non tech-savy people to try it out here.
                        </p>
                    </article>
                    <article class="p-4 w-11/12">
                        <h2 class="text-xl font-bold">Backstory</h2>
                        <p>
                            Since TreaTS are a project implementation of my
                            ongoing research paper, I had the urge to provide a
                            way to showcase it's usage somehow with somekind of
                            frontend.
                        </p>
                    </article>
                    <article class="p-4 w-11/12">
                        <h2 class="text-xl font-bold">Author</h2>
                        <p>Ronan Harris</p>
                        <ul>
                            <li>gitlab.com/ronanharris09</li>
                            <li>miteiruka@karakunai.com</li>
                        </ul>
                    </article>
                    <article class="p-4 w-11/12">
                        <h2 class="text-xl font-bold">License</h2>
                        <ul>
                            <li>
                                Name:{" "}
                                <strong>
                                    GNU General Public License v2.0 only
                                </strong>
                            </li>
                            <li>
                                SPDX-License-Identifier:{" "}
                                <strong>GPL-2.0-only</strong>
                            </li>
                            <li>
                                Full License Copy:{" "}
                                <a
                                    href="https://gitlab.com/ronanharris09/metriks/-/blob/master/COPYING"
                                    target="_blank"
                                    referrerpolicy="no-referrer"
                                >
                                    <strong>Visit Here</strong>
                                </a>
                            </li>
                        </ul>
                    </article>
                    <article class="p-4 w-11/12">
                        <h2 class="text-xl font-bold">Source Code</h2>
                        <p>gitlab.com/ronanharris09/metriks</p>
                    </article>
                </>
            </Dialog>
        </>
    )
}

AboutDialog.propTypes = propTypes

AboutDialog.defaultProps = {}

export default AboutDialog
