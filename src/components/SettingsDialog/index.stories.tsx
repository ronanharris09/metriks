import { h } from "preact"
import { Meta, Story } from "@storybook/preact"
import SettingsDialog, { Props } from "./"

export default {
    title: "Metriks/SettingsDialog",
    component: SettingsDialog,
} as Meta<typeof SettingsDialog>

const Template: Story<Props> = (args) => <SettingsDialog {...args} />

export const Default = Template.bind({})
Default.args = {}
