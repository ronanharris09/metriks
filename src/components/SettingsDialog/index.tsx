import { h, JSX, Fragment } from "preact"
import { useContext, useState } from "preact/hooks"
import PropTypes, { InferProps } from "prop-types"
import { StoreContext } from "../../context"
import { ACTION, ACCESSKEY } from "../../constants"
import Dialog from "../Dialog"

const propTypes = {}

export type Props = InferProps<typeof propTypes>

const SettingsDialog = (): JSX.Element => {
    const { store, dispatch } = useContext(StoreContext)

    const dialogStateHandler = () => {
        let prevState = store
        prevState.visual.dialog.settings = !prevState.visual.dialog.settings

        dispatch({
            type: ACTION.STORE.VISUAL,
            body: prevState,
        })
    }

    const onNameChange = (s: string) => {
        dispatch({
            type: ACTION.STORE.CURRENT,
            body: {
                ...store,
                current: {
                    ...store.current,
                    name: s,
                    date: new Date().toISOString(),
                },
            },
        })
    }

    const copyToSavedBuffer = () => {
        const filteredSave = store.saved.filter(
            (item) => item.name !== store.current.name
        )
        filteredSave.push(store.current)

        dispatch({
            type: ACTION.STORE.CURRENT,
            body: {
                ...store,
                saved: filteredSave,
            },
        })
    }

    return (
        <>
            <Dialog
                title="Settings"
                description="Manage file name, reset storage, or add current buffer to saved buffer"
                isOpen={store.visual.dialog.settings}
                callback={() => dialogStateHandler()}
                accessKey={ACCESSKEY.DIALOG.SETTINGS}
            >
                <>
                    <ul>
                        <li class="flex flex-col p-4" tabIndex={0}>
                            <label
                                class="font-bold mb-2"
                                for="dialog-settings-name"
                            >
                                Name
                            </label>
                            <input
                                class="p-4 border-2 border-gray-400"
                                type="text"
                                name="dialog-settings-name"
                                value={store.current.name}
                                onInput={(e) =>
                                    onNameChange(e.currentTarget.value)
                                }
                            />
                        </li>
                        <li class="flex flex-col p-4">
                            <button
                                class="p-4 font-bold border-t-2 border-gray-700 outline-none text-white bg-gray-700 hover:text-gray-900 hover:bg-gray-400 focus:text-gray-900 focus:bg-gray-400"
                                onClick={() => copyToSavedBuffer()}
                            >
                                Copy to Saved Buffer
                            </button>
                        </li>
                    </ul>
                </>
            </Dialog>
        </>
    )
}

SettingsDialog.propTypes = propTypes

SettingsDialog.defaultProps = {}

export default SettingsDialog
