import { h } from "preact"
import { Meta, Story } from "@storybook/preact"
import Dialog, { Props } from "./"

export default {
    title: "Metriks/Dialog",
    component: Dialog,
} as Meta<typeof Dialog>

const Template: Story<Props> = (args) => (
    <div class="bg-white relative w-full py-96">
        <Dialog {...args}>
            <h1>Test</h1>
        </Dialog>
    </div>
)

export const Default = Template.bind({})
Default.args = {
    isOpen: true,
}
