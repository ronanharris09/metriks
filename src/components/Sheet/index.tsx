import { h, JSX, Fragment } from "preact"
import { memo } from "preact/compat"
import { useState, useContext, useEffect } from "preact/hooks"
import PropTypes, { InferProps } from "prop-types"
import Column from "../Column"
import { StoreContext } from "../../context"
import { ACTION } from "../../constants"

const propTypes = {
    manualSaveTrigger: PropTypes.number.isRequired,
}

export type Props = InferProps<typeof propTypes>

const Sheet = ({ manualSaveTrigger }: Props): JSX.Element => {
    const { store, dispatch } = useContext(StoreContext)
    const [hoverIndex, setHoverIndex] = useState<number>(-1)
    const [localState, setLocalState] = useState<Array<Array<number>>>(
        store.current.data
    )

    const sheetStateHandler = (newValue: Array<number>, index: number) => {
        let merged = store.current.data
        merged[index] = newValue
        setLocalState(merged)

        if (store.visual.status.metriks === "WRITE") {
            dispatch({
                type: ACTION.STORE.CURRENT,
                body: {
                    ...store,
                    current: merged,
                },
            })
        }
    }

    const rowHoverHandler = (index: number) => {
        if (hoverIndex !== index) {
            setHoverIndex(index)
        }
    }

    useEffect(() => {
        if (store) {
            setLocalState(store.current.data)
        }
    }, [store.current.data])

    useEffect(() => {
        if (manualSaveTrigger > 0) {
            dispatch({
                type: ACTION.STORE.CURRENT,
                body: {
                    ...store,
                    current: {
                        name: store.current.name,
                        data: localState,
                        date: new Date().toISOString(),
                    },
                },
            })
        }
    }, [manualSaveTrigger])

    return (
        <>
            {localState.length === 0 && (
                <div class="flex justify-center items-center bg-white h-[540px] max-h-[540px]">
                    <article class="text-center text-gray-900">
                        <div class="rounded-full p-4 bg-gray-900 text-white mb-8 w-24 h-24 mx-auto">
                            <span class="text-7xl text-center">?</span>
                        </div>
                        <h2 class="text-5xl font-light mb-4">
                            Empty Worksheet
                        </h2>
                        <p class="text-xl font-medium">
                            Load saved data into current buffer, <br /> or add
                            rows & columns to start working with metriks
                        </p>
                    </article>
                </div>
            )}
            {localState.length > 0 && (
                <div
                    class="flex flex-col overflow-scroll bg-white h-[540px] max-h-[540px]"
                    onMouseLeave={() => rowHoverHandler(-1)}
                >
                    <div class="flex w-max">
                        <div class="h-full my-auto px-4 text-center w-16">
                            0
                        </div>
                        {localState.map((item, index) => (
                            <div
                                key={`Column-Metriks-Name-${index}`}
                                class={[
                                    "flex",
                                    hoverIndex === index ? "bg-slate-300" : "",
                                ].join(" ")}
                                onMouseOver={() => rowHoverHandler(index)}
                            >
                                <div class="h-full my-auto px-4 w-16 text-center">
                                    {index + 1}
                                </div>
                            </div>
                        ))}
                    </div>
                    {localState.map((item, index) => (
                        <div
                            key={`Column-Metriks-${index}`}
                            class="flex hover:bg-slate-300 w-max"
                        >
                            <div class="h-full my-auto px-4 w-16 text-center">
                                {index + 1}
                            </div>
                            <Column
                                rowData={item}
                                rowId={index}
                                rowCallback={(e, i) => sheetStateHandler(e, i)}
                                rowHover={(i) => rowHoverHandler(i)}
                                renderTrigger={store.current.date}
                            />
                        </div>
                    ))}
                </div>
            )}
        </>
    )
}

Sheet.propTypes = propTypes

Sheet.defaultProps = {
    manualSaveTrigger: 0,
    path: "/",
    to: "",
    saveIndex: "",
}

const memoCompare = (prevState: Props, nextState: Props): boolean => {
    return prevState.manualSaveTrigger === nextState.manualSaveTrigger
}

export default memo(Sheet, memoCompare)
