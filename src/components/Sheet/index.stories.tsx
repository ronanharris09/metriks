import { h } from "preact"
import { Meta, Story } from "@storybook/preact"
import Sheet, { Props } from "./"

export default {
    title: "Metriks/Sheet",
    component: Sheet,
} as Meta<typeof Sheet>

const Template: Story<Props> = (args) => <Sheet {...args} />

export const Default = Template.bind({})
Default.args = {}
