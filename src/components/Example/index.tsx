import { h, JSX, Fragment } from "preact"
import { useState, useContext } from "preact/hooks"
import PropTypes, { InferProps } from "prop-types"

const propTypes = {
    text: PropTypes.string.isRequired,
    callback: PropTypes.func.isRequired,
    children: PropTypes.node.isRequired,
}

export type Props = InferProps<typeof propTypes>

const Example = ({ text, callback, children }: Props): JSX.Element => {
    return (
        <>
            <h1>{text}</h1>
            <button type="button" onClick={() => callback()}>
                Click
            </button>
            {children}
        </>
    )
}

Example.propTypes = propTypes

Example.defaultProps = {
    text: "Lorem ipsum",
    callback: () => {},
    children: <></>,
}

export default Example
