import { h } from "preact"
import { Meta, Story } from "@storybook/preact"
import Example, { Props } from "./"

export default {
    title: "Metriks/Example",
    component: Example,
} as Meta<typeof Example>

const Template: Story<Props> = (args) => <Example {...args} />

export const Default = Template.bind({})
Default.args = {}
