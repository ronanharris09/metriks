import { h } from "preact"
import { Meta, Story } from "@storybook/preact"
import Column, { Props } from "./"

export default {
    title: "Metriks/Column",
    component: Column,
} as Meta<typeof Column>

const Template: Story<Props> = (args) => <Column {...args} />

export const Default = Template.bind({})
Default.args = {
    rowData: Array(3).fill(0),
    rowId: -1,
}
