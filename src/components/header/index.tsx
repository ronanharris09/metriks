import { FunctionalComponent, h, JSX } from "preact"
import { Link } from "preact-router/match"
import style from "./style.css"

const Header: FunctionalComponent = (): JSX.Element => {
    return (
        <header class={style.header}>
            <h1>Preact App</h1>
            <nav>
                <Link activeClassName="text-blue-300 bg-slate-700" href="/">
                    Home
                </Link>
                <Link
                    activeClassName="text-slate-300 bg-slate-700"
                    href="/profile"
                >
                    Me
                </Link>
                <Link activeClassName={style.active} href="/profile/john">
                    John
                </Link>
            </nav>
        </header>
    )
}

export default Header
