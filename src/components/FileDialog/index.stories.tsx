import { h } from "preact"
import { Meta, Story } from "@storybook/preact"
import FileDialog, { Props } from "./"

export default {
    title: "Metriks/FileDialog",
    component: FileDialog,
} as Meta<typeof FileDialog>

const Template: Story<Props> = (args) => <FileDialog {...args} />

export const Default = Template.bind({})
Default.args = {}
