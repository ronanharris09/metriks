import { h, JSX, Fragment } from "preact"
import { useContext, useState } from "preact/hooks"
import PropTypes, { InferProps } from "prop-types"
import { StoreContext } from "../../context"
import { ACTION, ACCESSKEY } from "../../constants"
import Dialog from "../Dialog"

const propTypes = {}

export type Props = InferProps<typeof propTypes>

const FileDialog = (): JSX.Element => {
    const { store, dispatch } = useContext(StoreContext)
    const [renderTrigger, setRenderTrigger] = useState<string>(
        store.current.date
    )

    const dialogStateHandler = () => {
        let prevState = store
        prevState.visual.dialog.file = !prevState.visual.dialog.file

        dispatch({
            type: ACTION.STORE.VISUAL,
            body: prevState,
        })
    }

    const loadToBuffer = (i: number) => {
        dispatch({
            type: ACTION.STORE.CURRENT,
            body: {
                ...store,
                current: {
                    ...store.saved[i],
                    date: new Date().toISOString(),
                },
            },
        })

        setTimeout(() => {
            window.location.reload()
        }, 300)
    }

    const removeFromBuffer = (i: number) => {
        const filteredSave = store.saved.filter((_, index) => index !== i)

        dispatch({
            type: ACTION.STORE.CURRENT,
            body: {
                ...store,
                saved: filteredSave,
            },
        })
    }

    return (
        <>
            <Dialog
                title="File Management"
                description="Manage saves or load one into the current buffer"
                isOpen={store.visual.dialog.file}
                callback={() => dialogStateHandler()}
                accessKey={ACCESSKEY.DIALOG.FILE}
            >
                <>
                    <ul tabIndex={0}>
                        {store.saved.map((item, index) => (
                            <li key={`SAVED-GRAPH-${index}`} tabIndex={0}>
                                <article class="flex justify-between p-4 border-2 border-gray-200 my-1">
                                    <h2 class="text-2xl font-light">
                                        {item.name.length > 24
                                            ? `${item.name.slice(0, 15)}...`
                                            : item.name}
                                    </h2>
                                    <div class="grid grid-cols-2 gap-4 ml-16">
                                        <button
                                            class="px-4 py-1 bg-gray-300 font-bold outline-none hover:bg-gray-900 hover:text-white focus:bg-gray-900 focus:text-white"
                                            onClick={() => loadToBuffer(index)}
                                        >
                                            Open
                                        </button>

                                        <button
                                            class="px-4 py-1 bg-gray-300 font-bold outline-none hover:bg-gray-900 hover:text-white focus:bg-gray-900 focus:text-white"
                                            onClick={() =>
                                                removeFromBuffer(index)
                                            }
                                        >
                                            Delete
                                        </button>
                                    </div>
                                </article>
                            </li>
                        ))}
                    </ul>
                </>
            </Dialog>
        </>
    )
}

FileDialog.propTypes = propTypes

FileDialog.defaultProps = {}

export default FileDialog
