import { h } from "preact"
import { Meta, Story } from "@storybook/preact"
import Cell, { Props } from "./"

export default {
    title: "Metriks/Cell",
    component: Cell,
    parameters: {
        actions: {
            handles: ["input"],
        },
    },
} as Meta<typeof Cell>

const Template: Story<Props> = (args) => <Cell {...args} />

export const Default = Template.bind({})
Default.args = {
    size: "single",
}

export const Double = Template.bind({})
Double.args = {
    size: "double",
}

export const Triple = Template.bind({})
Triple.args = {
    size: "triple",
}

export const Quadruple = Template.bind({})
Quadruple.args = {
    size: "quadruple",
}
