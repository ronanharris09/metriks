import { h, JSX, Fragment } from "preact"
import { useState, useCallback } from "preact/hooks"
import { memo } from "preact/compat"
import PropTypes, { InferProps } from "prop-types"
import style from "./style.css"

const propTypes = {
    isHover: PropTypes.func.isRequired,
    isDisabled: PropTypes.bool.isRequired,
    stepFormat: PropTypes.number.isRequired,
    identifier: PropTypes.number.isRequired,
    current: PropTypes.number.isRequired,
    size: PropTypes.oneOf<string>(["single", "double", "triple", "quadruple"])
        .isRequired,
    callback: PropTypes.func.isRequired,
}

export type Props = InferProps<typeof propTypes>

const dummyCallback = (value: number) => {
    if (process.env.NODE_ENV === "development") {
        if (value) {
            console.log(value)
        }
    }
}

const Cell = ({
    isHover,
    isDisabled,
    stepFormat,
    identifier,
    current,
    size,
    callback,
}: Props): JSX.Element => {
    const [value, setValue] = useState<number>(current)
    const onInputChange = useCallback((e: number) => callback(e), [callback])

    const changeHandler = (raw: number) => {
        const processed = Number(
            raw.toPrecision(Number(`${stepFormat}`.length - 2))
        )
        setValue(processed)
        onInputChange(processed)
    }

    const onHover = useCallback(() => isHover(identifier), [isHover])

    let width = "w-16"

    switch (size) {
        case "double":
            width = "w-32"
            break
        case "triple":
            width = "w-64"
            break
        case "quadruple":
            width = "w-80"
            break

        default:
            width = "w-16"
            break
    }

    return (
        <>
            <input
                type="number"
                class={[
                    width,
                    style,
                    "text-center border border-gray-200",
                    isDisabled ? "bg-gray-400" : "bg-white",
                ].join(" ")}
                placeholder="_"
                onInput={(e) => changeHandler(Number(e.currentTarget.value))}
                value={value}
                step={stepFormat}
                readonly={isDisabled}
                onMouseOver={onHover}
            />
        </>
    )
}

Cell.propTypes = propTypes

Cell.defaultProps = {
    isHover: (i: number) => console.log("CELL-HOVER", i),
    isDisabled: false,
    stepFormat: 0.001,
    identifier: -1,
    current: -1,
    size: "single",
    callback: dummyCallback,
}

const memoCompare = (prevState: Props, nextState: Props): boolean => {
    return (
        prevState.current === nextState.current &&
        prevState.identifier === nextState.identifier &&
        prevState.isDisabled === nextState.isDisabled &&
        prevState.size === nextState.size &&
        prevState.stepFormat === nextState.stepFormat
    )
}

export default memo(Cell, memoCompare)
