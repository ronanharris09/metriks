import { FunctionalComponent, h } from "preact"
import { useState } from "preact/hooks"
import { Route, Router } from "preact-router"
import Style from "../assets/style/index.css"
import Home from "../routes/home"
import Profile from "../routes/profile"
import NotFoundPage from "../routes/notfound"
import Header from "./header"
import { StoreProvider } from "../context"
import NavigationBar from "./NavigationBar"
import Sheet from "./Sheet"
import StatusBar from "./StatusBar"
import FileDialog from "./FileDialog"
import AboutDialog from "./AboutDialog"
import TreatsDialog from "./TreatsDialog"
import SettingsDialog from "./SettingsDialog"
import Redirect from "./Redirect"

const App: FunctionalComponent = () => {
    const [incrementalTrigger, setIncrementalTrigger] = useState<number>(0)

    return (
        <main id="preact_root" class={[Style, "pt-[92px]"].join(" ")}>
            <StoreProvider>
                <NavigationBar key="default-NavigationBar" />
                <Sheet
                    key="default-Sheet"
                    manualSaveTrigger={incrementalTrigger}
                />
                <StatusBar
                    key="default-StatusBar"
                    manualSaveCallback={() =>
                        setIncrementalTrigger((x) => x + 1)
                    }
                />
                <FileDialog key="default-FileDialog" />
                <TreatsDialog key="default-TreatsDialog" />
                <SettingsDialog key="default-SettingsDialog" />
                <AboutDialog key="default-AboutDialog" />
            </StoreProvider>
        </main>
    )
}

export default App
