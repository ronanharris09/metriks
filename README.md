NOTICE: README is a work in progress.

# metriks
A 2x2 spreadsheet built on top of Preact, TailwindCSS CLI, and Typescript as a frontend for one of my project [TreaTS](https://gitlab.com/ronanharris09/treats).

## CLI Commands
*   `npm prepare`: Initialize Husky
*   `npm install`: Installs dependencies
*   `npm run dev`: Run a development build with HMR server
*   `npm run serve`: Run a production-like server
*   `npm run build`: Production-ready tailwindcss (prebuild script) & preact build
*   `npm run lint`: Pass TypeScript files using ESLint
*   `npm run test`: Run Jest and Enzyme with
    [`enzyme-adapter-preact-pure`](https://github.com/preactjs/enzyme-adapter-preact-pure) for
    your tests
*   `npm run storybook`: Run a storybook instance at port 6006
*   `npm run build-storybook`: Build a static storybook instance to serve
*   `npm run tailwind`: Run tailwindcss-cli watcher to monitor class usages and build on change
*   `npm run build-tailwind`: Run tailwindcss-cli to build css once with minification


For detailed explanation on how things work, checkout the [CLI Readme(tps://github.com/developit/preact-cli/blob/master/README.md).

## License
This project is licensed under **`GNU General Public License v2.0 only`**. You may read the full copy of the license at `COPYING` file, or by visiting `https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html`. For further clarity, the **SPDX-License-Identifier** is **`GPL-2.0-only`**.

```txt
Copyright (C) 2021 Ronan Harris
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; version 2.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

```

